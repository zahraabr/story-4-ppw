from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.story3, name='story3'),
    path('profile/', views.portfolio, name='portfolio'),
    path('contact/', views.contact, name='contact'),
]