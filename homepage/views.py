from django.shortcuts import render

# Create your views here.
def story3(request):
    return render(request, 'story3.html')

def portfolio(request):
	return render(request, 'portfolio.html')

def contact(request):
	return render(request, 'contact.html')